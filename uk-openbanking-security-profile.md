# UK Open Banking OIDC Security Profile
## Introduction
In many cases, Fintech services such as aggregation services use screen scraping and store user passwords. This model is both brittle and insecure. To cope with the brittleness, it should utilize an API model with structured data and to cope with insecurity, it should utilize a token model such as OAuth [RFC6749, RFC6750].

Financial API aims to rectify the situation by developing a REST/JSON model protected by OAuth. However, just asking to use OAuth is too vague as there are many implementation choices. OAuth is a framework which can cover wide range of use-cases thus some implementation choices are easy to implement but less secure and some implementation choices are harder to implement but more secure. Financial services on the internet is a use-case that requires more secure implementation choices. That is, OAuth needs to be profiled to be used in the financial use-cases.

The OpenBanking Profile detailed below outlines the differences between the FAPI R+W profile with clauses and provisions necessary to reduce delivery risk for ASPSPs OP

### Notational Conventions
The key words "shall", "shall not", "should", "should not", "may", and "can" in this document are to be interpreted as described in ISO Directive Part 2. These key words are not used as dictionary terms such that any occurence of them shall be interpreted as key words and are not to be interpreted with their natural language meanings.
# Financial Services � Open Banking API Security Profile
This document is based on the [OpenID Foundations Financial API Read+Write specification document](http://openid.net/specs/openid-financial-api-part-2.html) which in turn is based on the Read only specification document. The OpenBanking profile will further shape these two base profiles in some cases tightening restrictions and in others loosening requirements using key words 
## 5. Read and Write API Security Profile
### 5.2  OB API Security Provisions
#### 5.2.1 Introduction
Open Banking's API Profile does not distinguish between the security requirements from a technical level between "read" and "write" resources. The security requirements for accessing PSU resources held at ASPSPs requires more protection level than a basic RFC6749 supports.

As a profile of The OAuth 2.0 Authorization Framework, this document mandates the following to the OpenBanking Financial APIs.

#### 5.2.2 Authorization Server
The Authorization Server

   * shall support confidential clients;
   * may support public clients;
   * shall support user authentication at appropriate level as defined in PSD2;
   * shall secure its token endpoint using MATLS;
   * shall require the `response_type` values `code` or `code id_token` or `code id_token token`;
   * shall authenticate the confidential client at the Token Endpoint using one of the following methods:
    1. `tls_client_auth` https://tools.ietf.org/html/draft-ietf-oauth-mtls-03 (Recommended); or
    2. `client_secret_basic` or `client_secret_post` provided the client identifier matches the client identifier bound to the underlying mutually authenticated TLS session (Allowed); or
    3. `private_key_jwt` or 'client_secret_jwt` (Recommended);
   * shall issue an ID Token in the token response when openid was included in the requested scope as in Section 3.1.3.3 of OIDC with its sub value corresponding to the "Intent Ticket ID"  and optional acr value in ID Token.
   * may support refresh tokens.

Further, if it wishes to provide the authenticated user's identifier to the client in the token response, the authorization server

   * shall issue an ID Token in the token response when openid was included in the requested scope as in Section 3.1.3.3 of [OIDC] with its sub value corresponding to the authenticated user and mandatory acr value in ID Token.
   * must support Request Objects passed by value as in clause 6.3 of OIDC.

#### 5.2.3 Public Client
OpenBanking OPs can support Public Clients at their discretion.

Should an OP wish to support public clients all provisions of the FAPI Read Write API security profile will be adhered too with exceptions below;

   * shall request user authentication at appropriate level as defined in PSD2 be that LoA 3 or 2 or other.

#### 5.2.4 Confidential Client
A Confidential Client

   * may use separate and distinct Redirect URI for each Authorization Server that it talks to;
   * shall accept signed ID Tokens; 

## 6. Accessing Protected Resources
### 6.2 Access provisions
#### 6.2.1 Protected resources provisions
The resource server with the FAPI endpoints

   * shall mandate mutually authenticated TLS 1.2 or later as defined in RFC5246 with the usage following the best practice in RFC7525;
   * shall verify that the client identifier bound to the underlying mutually authenticated TLS transport session matches the client that the access token was issued to;

### 6.3 Client provisions
The confidential client supporting this document

   * shall use mutually authenticated TLS 1.2 or later as defined in RFC5246 with the usage following the best practice in RFC7525;
   * shall supply the last time the customer logged into the client in the x-fapi-customer-last-logged-time header where the value is supplied as ** w3c date **, e.g., x-fapi-customer-last-logged-time: Tue, 11 Sep 2012 19:43:31 UTC; and
   * shall supply the customer�s IP address if this data is available or applicable in the x-fapi-customer-ip-address header, e.g., x-fapi-customer-ip-address: 198.51.100.119; and
   * shall send x-fapi-financial-id whose value is the unique identifier of the financial institution assigned by OpenBanking.

Further, the client

   * may supply the customers authentication context reference (ACR) or applicable in the x-fapi-customer-acr header, e.g., x-fapi-customer-acr; 

## 7. Request object endpoint
### 7.1 Introduction

   * OPs may not support `request_uri`, OIDC Request Object by Reference.
   * OPs must support Request Objects passed by value as in clause 6.3 of OIDC.

## 8. Security Considerations
### 8.1 TLS Considerations

The TLS considerations in FAPI Part 1 section 7.1 shall be followed.

### 8.2 Message source authentication failure and message integrity protection failure

It is not mandated that the Authorization request and response are authenticated. To provide message source authentication and integrity protection:

   1. Authorization servers should support the Request Object Endpoint as per FAPI part 2 clause 7
   1. Clients should Use request_uri (obtained from the Authorization Server) in the Authorization request as per FAPI part 2 clause 5.2.2.1
   1. Authorization servers should return an ID token as a detached signature ot the authorization response as per FAPI part 2 clause 5.2.2.3.

### 8.3 Message containment failure

The Message containment failure considerations in FAPI Part 1 section 7.4 shall be followed.
